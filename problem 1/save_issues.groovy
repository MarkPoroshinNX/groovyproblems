import groovy.xml.MarkupBuilder
import java.io.StringWriter
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Level
import org.apache.log4j.Logger
import java.text.SimpleDateFormat

log = Logger.getLogger("");
log.setLevel(Level.DEBUG);

def sw = new StringWriter()
def mb = new MarkupBuilder(sw)

def getProjectByKey(String key) {
    ComponentAccessor.projectManager.getProjectObjByKey(key)
}

def issues = ComponentAccessor.getIssueManager().getIssueObjects(
        ComponentAccessor.getIssueManager().getIssueIdsForProject(getProjectByKey("PFROM").getId())
)

def worklogManager = ComponentAccessor.getWorklogManager()
def dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
 
mb.issues() {
    issues.each() { i ->
        if (i.summary.startsWith("marked issue|")) {
            issue() {
                name(i)

                assignee(i.assignee)
                summary(i.summary)
                assigneeId(i.assigneeId)
                description(i.description)
                issueTypeName(i.issueType?.name)
                issueTypeId(i.issueType?.id)
                
                worklogManager.getByIssue(i).each { wl ->
                    worklog() {
                        log.debug(wl)
                        author(wl.authorKey)
                        comment(wl.comment)
                        date(dateFormat.format(wl.created))
                        timeSpent(wl.timeSpent)
                        startDate(dateFormat.format(wl.startDate))
                        
                    }
                }
            } 
        }
    }
}

def xml = sw.toString()
File file = new File("issues.xml")
file.newWriter()
file << xml
log.debug(xml)







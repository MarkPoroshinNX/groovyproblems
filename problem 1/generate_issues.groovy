import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.ConstantsManager
import com.atlassian.jira.issue.IssueFactory
import com.atlassian.jira.user.ApplicationUser
import org.apache.commons.lang.RandomStringUtils
import org.apache.log4j.Level
import org.apache.log4j.Logger



class IssueGenerator {
    final static NUMBER_OF_ISSUES = 1

    Logger log
    ConstantsManager constantManager
    IssueService issueService
    ApplicationUser user
    IssueFactory issueFactory

    IssueGenerator() {
        this.log = Logger.getLogger("");
        this.log.setLevel(Level.DEBUG);
        this.constantManager = ComponentAccessor.getConstantsManager()
        this.issueService = ComponentAccessor.getIssueService()
        this.user = ComponentAccessor.getJiraAuthenticationContext().loggedInUser
        this.issueFactory = ComponentAccessor.getIssueFactory()
    }

    def getProjectByKey(String key) {
    	ComponentAccessor.projectManager.getProjectObjByKey(key)
	}

    static def generateRandomString() {
        String charset = (('a'..'z') + ('0'..'9')).collect()
        Integer len = 9
        return RandomStringUtils.random(len, charset.toCharArray())
    }

    def generateRandomIssue(String prjKey) {
        def project = getProjectByKey(prjKey)
        def issueType = constantManager.getAllIssueTypeObjects().first()
        def issueInputParams = issueService.newIssueInputParameters()
        issueInputParams
                .setSummary("marked issue|" + generateRandomString())
                .setDescription("marked desc|" + generateRandomString())
                .setAssigneeId(user.getName())
                .setReporterId(user.getName())
                .setProjectId(project.getId())
                .setIssueTypeId(issueType.getId())
        		.addCustomFieldValue("customfield_10104", generateRandomString());
        IssueService.CreateValidationResult result = issueService.validateCreate(
                user,
                issueInputParams
        )
        if (result.getErrorCollection().hasAnyErrors()) {
            return 'failed:' + result.getErrorCollection()
        } else {
            issueService.create(user, result)
            return 'success'
        }
    }
    
    def generateRandomIssues(Integer numberOfIssues, String prjKey) {
        for (def i = 0; i < numberOfIssues; i++) {
            log.info(generateRandomIssue(prjKey))
        }
    }
}

def issueGenerator = new IssueGenerator()
issueGenerator.generateRandomIssues(5, "PFROM")



import groovy.util.XmlParser
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.ConstantsManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.worklog.WorklogManager
import com.atlassian.jira.issue.worklog.WorklogImpl
import com.atlassian.jira.bc.issue.worklog.WorklogInputParametersImpl
import java.text.SimpleDateFormat;
import java.sql.Timestamp
import org.apache.log4j.Level
import org.apache.log4j.Logger


class WorklogLoader {
    Logger log
    WorklogManager worklogManager
    IssueService issueService
    ApplicationUser user
    SimpleDateFormat formatter

    WorklogLoader() {
        this.log = Logger.getLogger("");
        this.log.setLevel(Level.DEBUG);
        this.issueService = ComponentAccessor.getIssueService()
        this.worklogManager = ComponentAccessor.getWorklogManager()
        this.user = ComponentAccessor.getJiraAuthenticationContext().loggedInUser
        this.formatter = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss"); 
    }
    /**
     * build worklog from input params and inject it into issue
     * @return worklog instance else
     */
    def createWorklog(issue, author, comment, timeSpent, startDate, createdDate) {
        //log.debug("${issue} ${author} ${comment} ${timeSpent} ${startDate} ${createdDate}")
        def worklog = new WorklogImpl(
            worklogManager,
            issue, 
            null,
            author,
            comment,
            formatter.parse(startDate),
            null,
            null,
            Long.valueOf(timeSpent)
        )
        return worklogManager.create(
        	user,
            worklog, 
            null, 
            false
        )
    }
}

class IssueLoader {
    Logger log
    ConstantsManager constantManager
    IssueService issueService
    ApplicationUser user
    
    IssueLoader() {
        this.log = Logger.getLogger("");
        this.log.setLevel(Level.DEBUG);
        this.constantManager = ComponentAccessor.getConstantsManager()
        this.issueService = ComponentAccessor.getIssueService()
        this.user = ComponentAccessor.getJiraAuthenticationContext().loggedInUser
    }
    
    def getProjectByKey(String key) {
        ComponentAccessor.projectManager.getProjectObjByKey(key)
    }

    /**
     * build issue from input params and inject it into project
     * @return null if issue wasn't created and issue instance else
     */
    def createIssue(summary, description, assigneeId, issueTypeId, projectId, customfield_10104) {
        def issueInputParams = issueService.newIssueInputParameters()
        issueInputParams
                    .setSummary(summary)
                    .setDescription(description)
                    .setAssigneeId(assigneeId)
                    .setReporterId(user.getName())
                    .setProjectId(projectId)
                    .setIssueTypeId(issueTypeId)
                    .addCustomFieldValue("customfield_10104", customfield_10104);
        IssueService.CreateValidationResult result = issueService.validateCreate(
            user,
            issueInputParams
        )
        if (result.getErrorCollection().hasAnyErrors()) {
            log.info('failed:' + result.getErrorCollection())
            return null
        } else {
            return issueService.create(user, result).getIssue()
        }
    }
}


def issueLoader = new IssueLoader()
def worklogLoader = new WorklogLoader()
def file = new File("issues.xml")
def text = file.text
def issues = new XmlParser().parseText(text)
issues.each {i ->
    def issue =
        issueLoader.createIssue(
            i.summary.text(),
            i.description.text(),
            i.assigneeId.text(),
            i.issueTypeId.text(),
            issueLoader.getProjectByKey("PTOP").getId(),
            "custom field"
        )
    i.worklog.each { wl ->
            worklogLoader.createWorklog(
            	issue,
                wl.author.text(), 
                wl.comment.text(), 
                wl.timeSpent.text(), 
                wl.startDate.text(), 
                wl.createdDate.text()
            )
        }
}




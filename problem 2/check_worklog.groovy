import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Level
import org.apache.log4j.Logger
import com.atlassian.mail.Email;
import com.atlassian.mail.queue.SingleMailQueueItem;

final TIMESPENT_DAY_LIMIT = 8

def longToHours(timespent) {
    return timespent / 3600
}
def hoursToLong(hours) {
    return hours * 3600
}

def sendErrorMail(adress) {
    Email email = new Email(adress);
    email.setFromName("system");
    email.setSubject("system message");
    email.setBody("ERROR: exceeded timeapent per day");
    email.setMimeType("text/html");
    SingleMailQueueItem smqi = new SingleMailQueueItem(email);
    ComponentAccessor.getMailQueue().addItem(smqi);
}

log = Logger.getLogger("");
log.setLevel(Level.DEBUG);

def todayTimespentForUser = [:]

def worklogManager = ComponentAccessor.getWorklogManager()
def updatedWorklogs = worklogManager.getWorklogsUpdatedSince(hoursToLong(TIMESPENT_DAY_LIMIT))
updatedWorklogs.each {wl ->
    if (wl.getAuthor() in todayTimespentForUser) {
        todayTimespentForUser[wl.getAuthor()] = todayTimespentForUser[wl.getAuthor()] + longToHours(wl.timeSpent)
    } else {
        todayTimespentForUser[wl.getAuthor()] = longToHours(wl.timeSpent)
    }
}
def userUtil = ComponentAccessor.getUserUtil()
todayTimespentForUser.each {author, timespent ->
    log.debug("${author}: ${timespent}") 
    if (timespent > TIMESPENT_DAY_LIMIT) {
        def curUser = userUtil.getUser(author)
        sendErrorMail(curUser.getEmailAddress())
        log.debug(curUser.getEmailAddress())
    }
}


return todayTimespentForUser
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import org.apache.log4j.Level
import org.apache.log4j.Logger
import java.sql.Timestamp


log = Logger.getLogger("");
log.setLevel(Level.DEBUG);
user = ComponentAccessor.getJiraAuthenticationContext().loggedInUser

def getCustomFieldByKey(String key) {
    ComponentAccessor.customFieldManager.getCustomFieldObjectByName(key)
}

def update(user, issue) {
    ComponentAccessor.issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
}

def getIssue(String key) {
    ComponentAccessor.issueManager.getIssueObject(key)
}

def getProjectByKey(String key) {
    ComponentAccessor.projectManager.getProjectObjByKey(key)
}

def getIssuesByPrjKey(String prjKey) {
    def issues = ComponentAccessor.getIssueManager().getIssueObjects(
        ComponentAccessor.getIssueManager().getIssueIdsForProject(getProjectByKey(prjKey).getId())
	)
    return issues
} 

def getStartDate(issue) {
    def startDateField = getCustomFieldByKey("Start Date")
    return issue.getCustomFieldValue(startDateField)
}

def isIssueIntersect(issue1, issue2) {
    def check = { i1, i2 -> 
        if (i1.getDueDate().after(getStartDate(i2)) && 
            i1.getDueDate().before(i2.getDueDate())) {
            return true
        }
        if (getStartDate(i1).after(getStartDate(i2)) && 
            getStartDate(i1).before(i2.getDueDate())) {
            return true
        }
        return false
    }
    return check(issue1, issue2) || check(issue2, issue1)
}
/**
 * count minimal shift for solve intersection by shift right issue
 * @return long presentation of timestamp
 */
def countShift(leftIssue, rightIssue) {
    return leftIssue.getDueDate().getTime() - getStartDate(rightIssue).getTime()
}
/**
 * increase due date and start date by the interval value
 * @param issue: target MutableIssue
 * @param interval: Long presentation of Timestamp
 * @return
 */
def shiftIssue(issue, Long interval) {
     issue.setDueDate(
     	new Timestamp(issue.getDueDate().getTime() + interval)
     )
    def startDateField = getCustomFieldByKey("Start Date")
    def  newStartDate = new Timestamp(
        getStartDate(issue).getTime() + interval
    )
    issue.setCustomFieldValue(
        startDateField, 
    	newStartDate
    )
}
/**
 * check other issues for intersections with the target and
 * shifts due date and start date if necessary for placement target issue
 * @param targetIssue - issue to place
 */
def placePriorityIssue(targetIssue) {
    def issues = getIssuesByPrjKey(targetIssue.getProjectObject().key)
    def maxShift = 0
    def isHighPriority = true
    issues.each { issue ->
        log.debug("${issue}:")
        if (issues == targetIssue || !isHighPriority) {
            return
        }
        if (issue.getPriority().compareTo(targetIssue.getPriority()) < 0 ) {
            isHighPriority = false
            return
        }
        if (isIssueIntersect(targetIssue, issue)) {
            def curShift = countShift(targetIssue, issue)
            log.debug("${issue} shift ${curShift}")
            maxShift = maxShift > curShift ? maxShift : curShift
        }
    }
    log.debug("maxshift ${maxShift} isHighPriority:${isHighPriority}")
    if (maxShift != 0 && isHighPriority) {
        issues.each { issue ->
            if (issues == targetIssue) {
                return
            }
            log.debug("${issue} was shifted")
        	shiftIssue(issue, maxShift)
            update(user, issue)
        }
    }
}

def issue = getIssue("PFROM-1")
log.debug("${issue} ${getStartDate(issue)}")
placePriorityIssue(issue)





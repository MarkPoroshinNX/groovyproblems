import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import org.apache.log4j.Level
import org.apache.log4j.Logger
import java.sql.Timestamp

log = Logger.getLogger("");
log.setLevel(Level.DEBUG);
user = ComponentAccessor.getJiraAuthenticationContext().loggedInUser

def update(user, issue) {
    ComponentAccessor.issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
}

def getStartTime(issue) {
    new Timestamp(issue.getDueDate().getTime() - issue.getEstimate())
}

def getProjectByKey(String key) {
    ComponentAccessor.projectManager.getProjectObjByKey(key)
}

def getIssuesByPrjKey(String prjKey) {
    def issues = ComponentAccessor.getIssueManager().getIssueObjects(
        ComponentAccessor.getIssueManager().getIssueIdsForProject(getProjectByKey(prjKey).getId())
	)
    return issues
} 

def isIssueIntersect(issue1, issue2) {
    if (issue1.getDueDate().after(getStartTime(issue2)) && 
        issue1.getDueDate().before(issue2.getDueDate())) {
        return true
    }
    if (getStartTime(issue1).after(issue2.getCreated()) && 
        getStartTime(issue1).before(issue2.getDueDate())) {
        return true
    }
    return false
}




def placeIssue(targetIssue, projectKey) {
    def issues = getIssuesByPrjKey(projectKey)
    log.debug("""try to place ${targetIssue} 
    due time:${targetIssue.getDueDate()} 
    createdtime:${targetIssue.getCreated()}
    starttime${getStartTime(targetIssue)}""")
    
    issues.each { issue ->
        if (issue == targetIssue) {
            return
        }
    	log.debug("""${issue} 
        due time:${issue.getDueDate()} 
        createdtime:${issue.getCreated()}
        starttime${getStartTime(issue)}""")
        if (isIssueIntersect(issue, targetIssue)) {
            log.debug("${issue} and ${targetIssue} are intersect")
            if (issue.getPriority().compareTo(targetIssue.getPriority()) < 0 ) {
                log.debug("${issue} priority is over ${targetIssue} prioriry")
                targetIssue.setDueDate(
                	new Timestamp(0).setTime(
                        Math.abs(
                            issue.getDueDate().getTime() - getStartTime(targetIssue).getTime()
                    	)
                    )
                )
                update(user, targetIssue)
                log.debug("""dates changed ${targetIssue} 
        due time:${targetIssue.getDueDate()} 
        createdtime:${targetIssue.getCreated()}
        starttime${getStartTime(targetIssue)}""")
                placeIssue(targetIssue, "PFROM");
            } else {
                log.debug("${targetIssue} priority is over ${issue} prioriry")
                issue.setDueDate(
                	new Timestamp(0).setTime(
                        Math.abs(
                        	targetIssue.getDueDate().getTime() - getStartTime(issue).getTime()
                        )
                    )
                )
                update(user, targetIssue)
                log.debug("""dates changed ${issue} 
        due time:${issue.getDueDate()} 
        createdtime:${issue.getCreated()}
        starttime${getStartTime(issue)}""")
                placeIssue(issue, "PFROM")
            }
        }
	}
}

def targetIssue = ComponentAccessor.getIssueManager().getIssueObject("PFROM-1")
placeIssue(targetIssue, "PFROM")
